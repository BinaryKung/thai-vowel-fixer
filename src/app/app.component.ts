import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  inputText = '';
  outputLine: any[] = [];

  public ngOnInit() {
    this.inputText = `กกกอด
กาาาก
พิัิัมดดพัิมพ์
พ้ัมก้ิน
กาิฟ
พาเไิส
ซ้้้้้อน
กาไมมมมดกดกาไดไากดฟกดมมมมมมะา
าาไม
สมาาไ`;
    this.cleanText();
  }

  showError() {
    this.outputLine = [];
    let result = this.inputText;
    const reg = /\S?(?:(?<!\S)[ฯะาำๅๆ]|[เ-ไ][ฯ-ฺ]+|[ฯะาำๅๆ][ฯ-ฺๅ็-์]+|([ฯ-ๅ็-์])\1+|[\u0E31\u0E34-\u0E3A\u0E47-\u0E4E][\u0E31\u0E34-\u0E3A\u0E47-\u0E4E]+)\S?/gm;
    this.highlightErrorText(result, result.match(reg));
  }

  cleanText() {
    this.outputLine = [];
    let result = this.inputText;
    const regDuplicate = /([ฯ-ๅ็-์])\1+/gm;
    result = result.replace(regDuplicate, '$1');
    const regMisplace = /([่้๊๋])([ัิีื])/gm;
    result = result.replace(regMisplace, '$2$1');
    const regMultiVowel = /\S?(?:(?<!\S)[ฯะาำๅๆ]|[เ-ไ][ฯ-ๅ็-์]+|[ฯะาำๅๆ][ฯ-ฺๅ็-์]+|[\u0E31\u0E34-\u0E3A\u0E47-\u0E4E][\u0E31\u0E34-\u0E3A\u0E47-\u0E4E]+)\S?/gm;
    const errorText = result.match(regMultiVowel);
    this.highlightErrorText(result, errorText);
  }

  private highlightErrorText(fullText: string, errorText: RegExpMatchArray | null) {
    let textInLine: any[] = [];
    let searchText = fullText;
    let a = 0;
    if (errorText) {
      errorText.forEach(
        str => {
          let b = searchText.search(str);
          let c = b + str.length;
          let tempAB = searchText.substring(a, b);
          let tempBC = searchText.substring(b, c);
          if (tempAB) {
            let splitNormal = tempAB.split('\n');
            if (splitNormal.length > 1) {
              this.handleNormalText(splitNormal, textInLine);
              textInLine = [];
              if (splitNormal[splitNormal.length - 1] !== '') {
                textInLine.push({
                  text: splitNormal[splitNormal.length - 1],
                  type: 'NORMAL'
                });
              }
            } else {
              if (splitNormal[0] === '') {
                this.outputLine.push(Object.assign([], textInLine));
                textInLine = [];
              } else {
                textInLine.push({
                  text: tempAB,
                  type: 'NORMAL'
                });
              }
            }
          }
          textInLine.push({
            text: tempBC,
            type: 'ERROR'
          });
          searchText = searchText.substring(c);
        }
      );
    }
    if (searchText) {
      let splitNormal = searchText.split('\n');
      if (splitNormal.length > 1) {
        this.handleNormalText(splitNormal, textInLine);
        textInLine = [];
        textInLine.push({
          text: splitNormal[splitNormal.length - 1],
          type: 'NORMAL'
        });
        this.outputLine.push(Object.assign([], textInLine));
      } else {
        textInLine.push({
          text: searchText,
          type: 'NORMAL'
        });
        this.outputLine.push(textInLine);
      }
    } else {
      if (textInLine.length > 0) {
        this.outputLine.push(Object.assign([], textInLine));
      }
    }
  }

  private handleNormalText(splitNormal: any[], textInLine: any[]) {
    for (let i = 0; i < splitNormal.length - 1; i++) {
      if (splitNormal[i] !== '') {
        textInLine.push({
          text: splitNormal[i],
          type: 'NORMAL'
        });
      }
      this.outputLine.push(Object.assign([], textInLine));
      textInLine = [];
    }
  }

  clear() {
    this.inputText = '';
    this.outputLine = [];
  }

}
